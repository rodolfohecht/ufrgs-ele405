# -*- coding: utf-8 -*-
"""
Created on Wed Apr  3 12:53:24 2019

@author: Rodolfo Hecht
"""
import datetime
import numpy as np
import matplotlib.pyplot as plt
import math
from numpy.linalg import inv
import pandas as pd


base_de_dados = pd.read_excel("IGPM.xls")    
data = base_de_dados['Data']
igpm = base_de_dados['IGPM']
seqdata = base_de_dados['seqdata']

# Declaração matriz com amostras 
matriz = np.zeros((360,2))

# Prepação dos vetores 
x = np.array(seqdata)
y = np.array(igpm)

# TRasnposição dos vetores
x = x.T
y = y.T

# Variáveis de contadores
linhas = 360
fig = 1


t = np.linspace(0,360,360)


# Número de parêmetros da variável
teta = 4

# Calculo tetachapeu para toda a curva
matriz = np.zeros((linhas,4))
for i in range (0,linhas):
    ajuste = 0
    for j in reversed(range (0,4)):
        matriz[i,ajuste] = math.pow(x[i],j)
        ajuste = ajuste + 1
 
    
#Função ajuste de curva
tetachapeu = matriz.T @ matriz
tetachapeu = inv(tetachapeu)
covariancia = tetachapeu
tetachapeu = tetachapeu@matriz.T
tetachapeu = tetachapeu@y

# Equação da curva
ft = (tetachapeu[0]*t*t*t) + (tetachapeu[1]*t*t) + (tetachapeu[2]*t) + (tetachapeu[3])

# Erro quadrático
v_teta = np.sum(igpm - ft)
v_teta = math.pow(v_teta,2)

print('Erro qudrático ',v_teta)

#Cálculo da covariância
covariancia =  covariancia*2*v_teta/len(igpm-4)

print('Covariancia completa',':', covariancia)



# IMpressão gráfica
plt.figure(fig)
#plt.subplot(2,1,1)
plt.plot(x,y,'.')
plt.xlabel('Sequência de meses')
plt.ylabel('Índice IGP-M')
plt.plot(t,ft,'b')
#plt.subplot(2,1,2)
plt.show()


# Calculo para as curvas quinquenias
# Declaração de variávies
ft_parcial = np.linspace(0,60,60)
t_parcial = np.linspace(0,60,60)

# Variáveis para Contadores 
inicio_linha = 0
linhas = 60
cov_amostra_reduzida=np.zeros((2,60))
contador = 0


# Loop para processar o quinquênio
for k in range (0,6):
    
    matriz_parcial = np.zeros((60,4))

    x_parcial = x[inicio_linha:linhas]
    y_parcial = y[inicio_linha:linhas]

    x_parcial = x_parcial.T
    y_parcial = y_parcial.T
   
    # Loop para cálculo do teta chapéu do quinquênio
    for m in range (0,60):
        ajuste = 0
        for l in reversed(range (0,4)):
            matriz_parcial[m,ajuste] = math.pow(t_parcial[m],l)
            ajuste = ajuste+1
    
    # Ajuste de contadores    
    linhas = linhas + 60
    inicio_linha = inicio_linha + 60
    ajuste = ajuste + 60    
    
    #Função ajuste de curva para o quinquênio
    tetachapeu_parcial = matriz_parcial.T @ matriz_parcial
    tetachapeu_parcial = inv(tetachapeu_parcial)
    covariancia_parcial = tetachapeu_parcial 
    tetachapeu_parcial = tetachapeu_parcial @ matriz_parcial.T
    tetachapeu_parcial = tetachapeu_parcial @ y_parcial

    # Equação da curva    
    ft_parcial = (tetachapeu_parcial[0]*t_parcial*t_parcial*t_parcial) + (tetachapeu_parcial[1]*t_parcial*t_parcial) + (tetachapeu_parcial[2]*t_parcial) + (tetachapeu_parcial[3])

    print('Teta chapeu quinquênio', tetachapeu_parcial)
    
#    Erro quadrático
    v_teta_parcial = np.sum(y_parcial - ft_parcial)
    v_teta_parcial = math.pow(v_teta_parcial,2)

    print('Erro qudrático ',v_teta)

    #Cálculo da covariância
    covariancia_parcial =  covariancia_parcial*2*v_teta_parcial/len(y_parcial-4)

    print('Covariancia quinquênio',':', covariancia)

    x_grafico = x[0:60]
    fig = fig+1
    plt.figure(fig)
#    plt.subplot(2,1,1)
    plt.plot(x_grafico,y_parcial,'.')
    plt.xlabel('Sequência de meses')
    plt.ylabel('Índice IGP-M')
    plt.plot(t_parcial,ft_parcial,'r')
#    plt.subplot(2,1,2)
#    plt.scatter(conv_amostra[0], conv_amostra[1])
    plt.show()
    
   