print("Cálculo do fatorial de um número\n")

    # leia o valor de n
n = int(input("Digite um número inteiro não-negativo: "))

    # inicializações
i     = 1  # contador
num_fat = 1  

    # calcule n!
while i <= n:
    num_fat=num_fat * i 
    i = i + 1

print("%d! = %d" %(n, num_fat))     