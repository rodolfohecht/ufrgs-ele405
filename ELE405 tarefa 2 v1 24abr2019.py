# -*- coding: utf-8 -*-
"""
Created on Wed Apr 24 09:15:35 2019

@author: rodol
"""
# Importanção de bibliotecas
import numpy as np
from numpy.linalg import inv
#import pandas as pd 
#import scipy
from scipy import signal
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
from math import sqrt

#Ações iniciais
plt.close("all") # Fechamento de janelas gráficas anteriores

# Definição de variáveis
N=5000 #N = Tamanho das amostras Vin 
t = np.arange(N) # Número de interações temporais N de Vin 
fig=0 # Numerador de gráficos plotados


Vin= 5 # Tensão contínua em Volts de entrada  
u = np.ones(N)*Vin # Dados de entrada u
u[0]=0
t = np.arange(N) 

h = 0.00001
R = 10**(3) #  Resistor (Ohm)
C = 10**(-6)  #  Capacitor (Farad)

a = 1 - h/(R*C) # Formulação do termo a da função de transferência 
b = h/(R*C) # Formulação do termo b da função de transferência

# Filtragem do Vout - sinal de saída ckt RC
y = signal.lfilter([b],[1, -a],u) # B/A

# Plotagem de Vin e Vout sem filtragem.
fig=fig+1
plt.figure(fig)
plt.plot(t,u,'k')
plt.plot(t,y,'b')
plt.title ('Vin, Vout - Dados inicias')

# Geração do ruído branco
r = np.random.randn(N)/100

# Filtragem do ruído branco
v = signal.lfilter([1],[1, -a],r)

# Definição yr = Variável que acumulará y + r (ruído) 
yr =  y +  v

# Criando a phi
phi = np.zeros((N, 2))

# Criação da Matriza N linhas e 2 colunas para acumulação de U e Yr
for i in range(1,N):
    phi[i,0] = u[i]
    phi[i,1] = -yr[i-1] # Utilização de i-1 para utilização na predição de i seguinte
    
# Função ajuste de curva
theta_chapeu = phi.T @ phi
theta_chapeu = inv(theta_chapeu)
theta_chapeu = theta_chapeu@phi.T
theta_chapeu = theta_chapeu@yr

# Preparação da simulação
numerador = np.array([theta_chapeu[0],0])
denominador = np.array([1,theta_chapeu[1]])
y_simulado = signal.lfilter(numerador,denominador,u)

# Cálculo da predição
y_pred = signal.lfilter([theta_chapeu[0]],np.array([1]),u) - signal.lfilter(denominador,np.array([1]),yr) + yr

# Plotagem tempo, y predicto, y simulado
fig=fig+1
plt.figure(fig)
plt.plot(t,y_pred,'g')
plt.plot(t,yr,'r')
plt.plot(t,y_simulado,'b')
plt.title ('Vout - Simulado (vermelho) Predicto(verde)')  



rms = sqrt(mean_squared_error(yr, y_pred))
print(rms)
